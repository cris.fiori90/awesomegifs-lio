#!/bin/bash
rm Dockerfile
touch Dockerfile
echo -e "FROM node:16 \n
WORKDIR /usr/src/app \n
COPY package*.json ./ \n
RUN npm install \n
COPY . .  \n
EXPOSE 5173 \n
CMD ['"npm"','"run"','"dev"']" >> Dockerfile

